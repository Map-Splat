#!perl
use 5.006;
use strict;
use warnings FATAL => 'all';
use Test::More;

if ( $ENV{RELEASE_TESTING} ) {

  plan tests => 2;

  use Map::Splat;
  my $map = Map::Splat->new(
    lon => -122.279,
    lat => 37.939,
    height => 300,
    freq => 2460,
    azimuth => 200,
    h_width => 180,
    v_width => 20,
    tilt => 2,
    max_loss => 180,
    min_loss => 30,
  );
  $map->calculate;
  ok( defined $map->box, 'bounding box exists' );
  ok( length($map->png) > 0, 'PNG generated' );

} else {

  plan( skip_all => 'Operational testing not required to build');

}
